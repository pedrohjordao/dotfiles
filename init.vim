if empty(glob('~/.vm/tmp'))
    silent !mkdir -p ~/.vim/tmp
endif
set directory=$HOME/.vim/tmp
" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
if !exists('g:vscode')
call plug#begin('~/.nvim/plugged')
Plug 'preservim/nerdcommenter'
" Use release branch (Recommend)
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Plugin for scala development
Plug 'derekwyatt/vim-scala'

" Git plugins
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'tommcdo/vim-fubitive'
Plug 'shumphrey/fugitive-gitlab.vim'
Plug 'tpope/vim-rhubarb'
Plug 'junegunn/gv.vim'
Plug 'rhysd/git-messenger.vim'

" Rust plugin
Plug 'rust-lang/rust.vim'

"Fsharp plugin
Plug 'kongo2002/fsharp-vim'

" FZF
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'antoinemadec/coc-fzf'
Plug 'stsewd/fzf-checkout.vim'

"Airline
" Plug 'vim-airline/vim-airline'
Plug 'hardcoreplayers/spaceline.vim'
" Use the icon plugin for better behavior
Plug 'ryanoasis/vim-devicons'

" Colorscheme
Plug 'morhetz/gruvbox'

" leader mappings
Plug 'liuchengxu/vim-which-key'

Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'

Plug 'airblade/vim-rooter'
" Auto tab setting
Plug 'tpope/vim-sleuth'

" Debugging
Plug 'puremourning/vimspector'

Plug 'tpope/vim-eunuch'

Plug 'benmills/vimux'

Plug 'joereynolds/SQHell.vim'

Plug 'machakann/vim-highlightedyank'
"" Initialize plugin system
call plug#end()

"------------------------------- General Settings ----------------------------
set encoding=UTF-8
filetype plugin indent on
syntax on
set wildmenu
set confirm
set hls is
set ignorecase
set smartcase
set colorcolumn=120
set expandtab
set mouse=a
set cursorline
" true terminal colors	
set termguicolors
" colorscheme
colorscheme gruvbox
" font
set guifont=Hasklug\ Nerd\ Font:h11
" show line number
set number
set tabstop=2                           " Insert 2 spaces for a tab
set shiftwidth=2                        " Change the number of space characters inserted for indentation
set smarttab                            " Makes tabbing smarter will realize you have 2 vs 4
set expandtab                           " Converts tabs to spaces
set smartindent                         " Makes indenting smart

" if hidden is not set, TextEdit might fail.
set hidden
set autowriteall

" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Better display for messages
set cmdheight=1

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c 

" always show signcolumns
set signcolumn=yes

set showmatch " show matching brackets
set showcmd
" Highlight matching pairs of brackets. Use the '%' character to jump between
" them.
set matchpairs+=<:>


" Display different types of white spaces.
set list
set listchars=tab:›\ ,trail:•,extends:#,nbsp:.,space:-

"------------------------------------------------------------------------------

nnoremap <SPACE> <Nop>
let g:mapleader="\<Space>"

" make current window as wide as possible in both directions
noremap <leader>o :res<CR>:vertical res<CR>
noremap <leader>O :only<CR>

" ctrl-d erases the next char
inoremap <C-d> <Del>
"
" Changes <C-c> behavior to have the same meaning as <Esc>
" This is significat to make block insert work as expected
inoremap <C-c> <Esc>

" Better indenting
vnoremap < <gv
vnoremap > >gv

" lower bar
let g:spaceline_seperate_style= 'curve'
let g:spaceline_colorscheme = 'space'


source $HOME/.config/nvim/plug-configs/coc.vim
source $HOME/.config/nvim/plug-configs/git.vim
source $HOME/.config/nvim/plug-configs/fzf.vim
source $HOME/.config/nvim/plug-configs/which-key.vim
source $HOME/.config/nvim/plug-configs/nerdcommenter.vim
source $HOME/.config/nvim/plug-configs/eunuch.vim
source $HOME/.config/nvim/plug-configs/vimspector.vim
source $HOME/.config/nvim/plug-configs/fzfcoc.vim
source $HOME/.config/nvim/plug-configs/vimux.vim
source $HOME/.config/nvim/lazy-functions.vim

" Spell checking 
autocmd FileType gitcommit setlocal spell
autocmd FileType markdown setlocal spell
autocmd BufNewFile,BufRead Jenkinsfile set syntax=groovy

" Help Vim recognize *.sbt and *.sc as Scala files
au BufRead,BufNewFile *.sbt,*.sc set filetype=scala

command ExecSh set splitright | vnew | set filetype=sh | read !sh #
command ExecAmm set splitright | vnew | set filetype=sh | read !amm #
endif
