" Git remaps
noremap <leader>gv :GV<CR>
noremap <leader>gg :G<CR>
noremap <leader>gP :Gpush -u origin HEAD<CR>
noremap <leader>gF :Gpush --force<CR>
noremap <leader>gp :Gpull<CR>
noremap <leader>gl :Glog<CR>
noremap <leader>gr :Gread<CR>
noremap <leader>gR :Grebase 
noremap <silent> <leader>gb :GBranches<CR>
noremap <silent> <leader>gB :Gblame<CR>
noremap <leader>gc :Git checkout
noremap <leader>gw :Gwrite!<CR>
noremap <silent> <leader>gm :GitMessenger<CR>
" Fugitive Conflict Resolution
nnoremap <leader>gd :Gvdiff<CR>
nnoremap gdh :diffget //2<CR>
nnoremap gdl :diffget //3<CR>
