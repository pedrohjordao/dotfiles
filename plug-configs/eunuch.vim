noremap <leader>ed :Delete<CR>
noremap <leader>em :Move
noremap <leader>er :Rename
noremap <leader>eM :Mkdir
noremap <leader>eW :SudoWrite<CR>
noremap <leader>eE :SudoEdit<CR>
