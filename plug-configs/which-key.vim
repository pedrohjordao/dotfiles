call which_key#register('\', "g:which_key_map")
nnoremap <silent> <leader> :WhichKey '\'<CR>

" By default timeoutlen is 1000 ms
set timeoutlen=300

let g:which_key_map = {}

let g:which_key_map.p = 'files'
let g:which_key_map.b = 'buffers'

let g:which_key_map.o = 'maximize-split'
let g:which_key_map.O = 'only'

" Go To
let g:which_key_map['G'] = {
\ 'name' : '+goto',
\ '[' : ['G[', 'diagnostic prev'],
\ ']' : ['G]', 'diagnostic next'],
\ 'd' : ['Gd',  'definition'],
\ 'y' : ['Gy',  'type definition'],
\ 'i' : ['Gi',  'implementation'],
\ 'r' : ['Gr',  'references'],
\}

let g:which_key_map['g'] = {
\ 'name' : '+Git',
\ 'v' : ['gv', 'commit browser'],
\ 'g' : ['gg', 'status'],
\ 'P' : ['gP',  'push'],
\ 'F' : ['gF',  'force push'],
\ 'p' : ['gp',  'pull'],
\ 'l' : ['gl',  'log'],
\ 'r' : ['gr',  'read current buffer'],
\ 'R' : ['gR',  'rebase'],
\ 'b' : ['gb',  'Merginal'],
\ 'B' : ['gB',  'blame'],
\ 'c' : ['gc',  'checkout'],
\ 'd' : ['gd',  'open diff'],
\ 'w' : ['gw',  'write current buffer'],
\ 'm' : ['gm',  'commit message'],
\}


let g:which_key_map.K = 'documentation'
let g:which_key_map.f = 'search'
"
" code actions
let g:which_key_map['a'] = {
\ 'name' : '+action',
\ 'a' : ['aa', 'action selected'],
\ 'c' : ['ac', 'action line'],
\ 'w' : ['aw', 'action word'],
\ 'f' : ['af',  'fix current'],
\ 'l' : ['al',  'codelens'],
\ 'd' : ['ad',  'diagnostics'],
\ 'b' : ['ab',  'diagnostics current buffer'],
\ 'm' : ['am',  'commands'],
\ 'o' : ['ao',  'outline'],
\ 's' : ['as',  'symbols'],
\ 'r' : ['ar',  'rename symbol'],
\}

let g:which_key_map['d'] = {
\ 'name' : '+debug',
\ 'c' : ['dc', 'continue'],
\ 's' : ['ds', 'stop'],
\ 'r' : ['dr',  'restart'],
\ 'p' : ['dp',  'pause'],
\ 'b' : ['db',  'toggle breakpoint'],
\ 'B' : ['dB',  'conditional breakpoint'],
\ 'f' : ['df',  'function breakpoint'],
\ 'o' : ['do',  'step over'],
\ 'i' : ['di',  'step into'],
\ 'u' : ['du',  'step out'],
\}


let g:which_key_map['e'] = {
\ 'name' : '+filesystem',
\ 'd' : ['ed', 'delete'],
\ 'm' : ['em', 'move'],
\ 'r' : ['er',  'rename'],
\ 'M' : ['eM',  'mkdir'],
\ 'W' : ['eW',  'sudo write'],
\ 'E' : ['eE',  'sudo edit'],
\}


let g:which_key_map['t'] = {
\ 'name' : '+metals',
\ 'v' : ['tv', 'tree view'],
\ 'b' : ['tb', 'build' ],
\ 'p' : ['tp', 'packages' ],
\ 'c' : ['tc', 'compile' ],
\ 'f' : ['tf', 'current class' ],
\}

let g:which_key_map['v'] = {
\ 'name' : '+run',
\ 'p' : ['vp', 'prompt'],
\ 'q' : ['vq', 'close runner'],
\ 'l' : ['vl', 'run last command'],
\ 'i' : ['vi', 'inspect'],
\}
