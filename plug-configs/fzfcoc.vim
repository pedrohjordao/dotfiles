" Using CocList
" Show all diagnostics
nnoremap <silent> <leader>ad  :<C-u>CocFzfList diagnostics<cr>
nnoremap <silent> <leader>ab  :<C-u>CocFzfList diagnostics --current-buf<cr>
" Show commands
nnoremap <silent> <leader>am  :<C-u>CocFzfList commands<cr>
" Find symbol of current document
nnoremap <silent> <leader>ao  :<C-u>CocFzfList outline<cr>
" Search workspace symbols
nnoremap <silent> <leader>as  :<C-u>CocFzfList symbols<cr>
